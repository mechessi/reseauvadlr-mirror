<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OuvertureDossier extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(isset($_SESSION['idPatient']))
		{
			$data = array();
			$data['row'] = $this->patients_Modele->getPatientById($_SESSION['idPatient']);
			$data['autorisation'] = $this->patients_Modele->getAutorisation($_SESSION['idPatient']);
			$this->load->view('patientView', $data);
		}
		else
		{
			$this->load->view('erreurPatientView');
		}
	}

	public function recherchePatient()
	{
		$_POST['search'] = $this->input->get('recherche');
		$this->form_validation->set_rules('search',  '"Search"', 'trim|encode_php_tags|xss_clean');

		$tabCar = array(" ", "\t", "\n", "\r", "\0", "\x0B", "\xA0", '"');
		$recherche = str_replace($tabCar, array(), $_POST['search']);

		if ($this->form_validation->run())
		{
			$data = array();
			$data['patients'] = $this->patients_Modele->getAllPatientMatchWith($recherche);

			if(count($data['patients']) != 0)
			{
				$this->load->view('searchFileView', $data);
			}
			else
			{
				$this->load->view('notFoundPatient');
			}
		}
	}

	public function dossierPatient()
	{
		if($this->input->post('searchSubmit'))
		{
			$this->form_validation->set_rules('idPatient',  '"IDPatient"', 'trim|encode_php_tags|xss_clean');
			if ($this->form_validation->run())
			{
				$_SESSION['idPatient'] = $this->input->post('idPatient');
				$data = array();
				$data['row'] = $this->patients_Modele->getPatientById($_SESSION['idPatient']);
				$data['autorisation'] = $this->patients_Modele->getAutorisation($_SESSION['idPatient']);
				$this->load->view('patientView', $data);
			}
			else
			{
				$this->index();
			}
		}
		else
		{
			$this->index();
		}
	}

}