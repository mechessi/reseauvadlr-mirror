<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signaletique extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(isset($_SESSION['idPatient']))
		{
			$data = array();
			$data['row'] = $this->patients_Modele->getPatientById($_SESSION['idPatient']);
			$this->load->view('signaletiqueFinView', $data);
		}
		else
		{
			$this->load->view('erreurPatientView');
		}
	}

	public function ajoutInformations()
	{
		if($this->input->post('saveSignaletique'))
		{
			foreach($_POST as $p) {
    			$this->form_validation->set_rules($p, ucfirst($p), 'trim|encode_php_tags|xss_clean');
			}

			if ($this->form_validation->run())
			{
				if($this->input->post('Field1')) { $this->patients_Modele->setPathologie($_SESSION['idPatient'], $this->input->post('Field1')); }
				if($this->input->post('Field2')) { $this->patients_Modele->setRepresentant($_SESSION['idPatient'], $this->input->post('Field2')); }
				if($this->input->post('Field3')) { $this->patients_Modele->setRepresentant($_SESSION['idPatient'], $this->input->post('Field3')); }
				if($this->input->post('Field4')) { $this->patients_Modele->setDomicile($_SESSION['idPatient'], $this->input->post('Field4')); }
				if($this->input->post('Field9')) { $this->patients_Modele->setSituation($_SESSION['idPatient'], $this->input->post('Field9')); }
				if($this->input->post('Field18')) { $this->patients_Modele->setSituationMere($_SESSION['idPatient'], $this->input->post('Field18')); }
				if($this->input->post('Field19')) { $this->patients_Modele->setSituationPere($_SESSION['idPatient'], $this->input->post('Field19')); }
				if($this->input->post('Field20')) { $this->patients_Modele->setFratrie($_SESSION['idPatient'], $this->input->post('Field20')); }
				if($this->input->post('Field37')) { $this->patients_Modele->setRefus($_SESSION['idPatient'], $this->input->post('Field37')); }

				$this->patients_Modele->ajoutInfos($_SESSION['idPatient'], $this->input->post('Field5'),
				$this->input->post('Field6'), $this->input->post('Field7'), $this->input->post('Field8'), 
				$this->input->post('Field10'), $this->input->post('Field11'), $this->input->post('Field12'), 
				$this->input->post('Field13'), $this->input->post('Field14'), $this->input->post('Field15'),
				$this->input->post('Field16'), $this->input->post('Field17'), $this->input->post('Field21'),
				$this->input->post('Field22'), $this->input->post('Field23'), $this->input->post('Field24'),
				$this->input->post('Field25'), $this->input->post('Field26'), $this->input->post('Field27'),
				$this->input->post('Field28'), $this->input->post('Field29'), $this->input->post('Field30'),
				$this->input->post('Field31'), $this->input->post('Field32'), $this->input->post('Field33'),
				$this->input->post('Field34'), $this->input->post('Field35'), $this->input->post('Field36'),
				$this->input->post('AAH'), $this->input->post('RQTH'), $this->input->post('AEEH'),
				$this->input->post('PCH'), $this->input->post('CARTES'), $this->input->post('ESMS'), 
				$this->input->post('fragSociale'));

				redirect('ouvertureDossier', 'refresh');
			}
			else
			{
				$this->index();
			}
		}
	}

}