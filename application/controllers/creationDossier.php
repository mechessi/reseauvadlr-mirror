<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CreationDossier extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('signaletiqueDepartView');
	}

	public function ajoutPatient()
	{
		if($this->input->post('saveForm')) {
			$this->form_validation->set_rules('Field1-1',  '"Prénom"',  'trim|encode_php_tags|xss_clean');
			$this->form_validation->set_rules('Field1-2', '"Nom"', 'trim|encode_php_tags|xss_clean');
			$this->form_validation->set_rules('Field4-1', '"JNaissance"', 'trim|encode_php_tags|xss_clean');
			$this->form_validation->set_rules('Field4-2', '"MNaissance"', 'trim|encode_php_tags|xss_clean');
			$this->form_validation->set_rules('Field4-3', '"ANaissance"', 'trim|encode_php_tags|xss_clean');
			$this->form_validation->set_rules('Field5', '"Département"', 'trim|encode_php_tags|xss_clean');
			$this->form_validation->set_rules('Field6', '"Numero"', 'trim|encode_php_tags|xss_clean');

			if ($this->form_validation->run())
			{
				$tabCar = array(" ", "\t", "\n", "\r", "\0", "\x0B", "\xA0", '"');
				$prenom = str_replace($tabCar, array(), $this->input->post('Field1-1'));
				$nom = str_replace($tabCar, array(), $this->input->post('Field1-2'));
				$jNaissance = str_replace($tabCar, array(), $this->input->post('Field4-1'));
				$mNaissance = str_replace($tabCar, array(), $this->input->post('Field4-2'));
				$aNaissance = str_replace($tabCar, array(), $this->input->post('Field4-3'));
				$departement = str_replace($tabCar, array(), $this->input->post('Field5'));
				$numero = str_replace($tabCar, array(), $this->input->post('Field6'));

				$alreadyCreated = $this->patients_Modele->ajouter_patient($prenom,
				$nom, $this->input->post('Field2-1'), $this->input->post('Field3'), $jNaissance, $mNaissance, 
				$aNaissance, $departement, $numero, $this->input->post('Field7'));

				if(!$alreadyCreated)
				{
					$data = array();
					$data['alreadyCreated'] = 'Patient déjà ajouté !';
					$this->load->view('signaletiqueDepartView', $data);
				}
				else
				{
					$_SESSION['idPatient'] = $this->patients_Modele->getLastId();
					redirect('ouvertureDossier', 'refresh');
				}
			}
			else
			{
				$this->index();
			}
		}
	}
	
}