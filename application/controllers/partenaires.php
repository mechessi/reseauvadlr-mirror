<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Partenaires extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array();
		$data['partenairesInitiaux'] = $this->partenaires_Modele->getAllPartenairesInitiaux($_SESSION['idPatient']);
		$data['partenairesImpliques'] = $this->partenaires_Modele->getAllPartenairesImpliques($_SESSION['idPatient']);
		$data['allPartenaires'] = $this->partenaires_Modele->getAllPartenaires();
		$this->load->view('partenairesView', $data);
	}

	public function insertionPartenaire()
	{
		$this->form_validation->set_rules('structure',  '"Structure"', 'trim|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('type',  '"Type"', 'trim|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('nom',  '"Nom"', 'trim|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('qualite',  '"Qualite"', 'trim|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('coordonnees',  '"Coordonnees"', 'trim|encode_php_tags|xss_clean');


		if ($this->form_validation->run())
		{
			$tabCar = array('"');
			$structure = str_replace($tabCar, array(), $this->input->post('structure'));
			$nom = str_replace($tabCar, array(), $this->input->post('nom'));
			$qualite = str_replace($tabCar, array(), $this->input->post('qualite'));
			$coordonnees = str_replace($tabCar, array(), $this->input->post('coordonnees'));

			if($this->input->post('submitInit'))
			{
				$alreadyCreated = $this->partenaires_Modele->ajoutPartenairesInitiaux($_SESSION['idPatient'], $structure, 
					$this->input->post('type'), $nom, $qualite, $coordonnees);
				if(!$alreadyCreated)
				{
					$data = array();
					$data['alreadyCreated'] = 'Partenaire déjà ajouté !';
					$data['partenairesInitiaux'] = $this->partenaires_Modele->getAllPartenairesInitiaux($_SESSION['idPatient']);
					$data['partenairesImpliques'] = $this->partenaires_Modele->getAllPartenairesImpliques($_SESSION['idPatient']);
					$data['allPartenaires'] = $this->partenaires_Modele->getAllPartenaires();
					$this->load->view('partenairesView', $data);
				}
				else
				{
					$this->index();
				}
			}
			else
			{
				$alreadyCreated = $this->partenaires_Modele->ajoutPartenairesImpliques($_SESSION['idPatient'], $structure, 
					$this->input->post('type'), $nom, $qualite, $coordonnees);
				if(!$alreadyCreated)
				{
					$data = array();
					$data['alreadyCreated'] = 'Partenaire déjà ajouté !';
					$data['partenairesInitiaux'] = $this->partenaires_Modele->getAllPartenairesInitiaux($_SESSION['idPatient']);
					$data['partenairesImpliques'] = $this->partenaires_Modele->getAllPartenairesImpliques($_SESSION['idPatient']);
					$data['allPartenaires'] = $this->partenaires_Modele->getAllPartenaires();
					$this->load->view('partenairesView', $data);
				}
				else
				{
					$this->index();
				}
			}
		}
		else
		{
			$this->index();
		}
	}

}