<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Complements extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array();
		$data['complements'] = $this->complements_Modele->getAllComplements($_SESSION['idPatient']);
		$this->load->view('complementsView', $data);
	}

	public function ajoutElements()
	{
		if($this->input->post('saveCompl'))
		{
			$this->form_validation->set_rules('Field1',  '"Elements"', 'trim|encode_php_tags|xss_clean');
			$this->form_validation->set_rules('Field2',  '"Problemes"', 'trim|encode_php_tags|xss_clean');
			$this->form_validation->set_rules('Field3',  '"Contenu"', 'trim|encode_php_tags|xss_clean');

			if ($this->form_validation->run())
			{
				$this->complements_Modele->insertComplements($_SESSION['idPatient'], $this->input->post('Field1'),
					$this->input->post('Field2'), $this->input->post('Field3'));
				redirect('ouvertureDossier', 'refresh');
				
			}
			else
			{
				$this->index();
			}
		}
	}

}