<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->config->item('charset'); ?>" />
		<link rel="icon" type="image/ico" href="<?php echo img_url('Logo_DROITE.ico'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('structure'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('form'); ?>" />
	</head>
    
	<body id="public">
	<div id="container" class="ltr">

	<form id="FormContact" class="wufoo topLabel page" accept-charset="UTF-8" enctype="multipart/form-data" 
	method="post" action="<?php echo site_url(); ?>complements/ajoutElements" autocomplete="off">

	<header id="header" class="info">
		<h2>Eléments complémentaires caractérisant la situation</h2>
	</header>

	<ul>
		<li id="foli1" class="notranslate">
			<label class="desc" id="title1" for="Field1">
				Eléments contextuels de la situation du patient
			</label>

			<div>
				<textarea id="Field1" name="Field1" class="field textarea medium" spellcheck="true" rows="10" cols="50" ><?php if(!empty($complements)) echo trim($complements->elements); ?></textarea>
			</div>
		</li>

		<li id="foli2" class="notranslate">
			<label class="desc" id="title2" for="Field2">
				Problèmes ou situation active ayant motivé la demande
			</label>

			<div>
				<textarea id="Field2" name="Field2" class="field textarea medium" spellcheck="true" rows="10" cols="50" ><?php if(!empty($complements)) echo trim($complements->problemes); ?></textarea>
			</div>
		</li>

		<li id="foli3" class="notranslate">
			<label class="desc" id="title3" for="Field3">
				Contenu de la demande
			</label>

			<div>
				<textarea id="Field3" name="Field3" class="field textarea medium" spellcheck="true" rows="10" cols="50" ><?php if(!empty($complements)) echo trim($complements->contenu); ?></textarea>
			</div>
		</li>

		<li class="buttons ">
			<div>
				<input id="saveCompl" name="saveCompl" class="btTxt submit" type="submit" value="Confirmer"/>
			</div>
		</li>
		
	</ul>
	</form> 

	</div><!--container-->
	</body>

</html>