<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->config->item('charset'); ?>" />
		<link rel="icon" type="image/ico" href="<?php echo img_url('Logo_DROITE.ico'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('structure'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('form'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('table'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('jquery-ui'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('jquery.toastmessage'); ?>" />
		<script src="<?php echo js_url('jquery-1.9.1'); ?>" type="text/javascript"></script>
		<script src="<?php echo js_url('jquery-ui'); ?>" type="text/javascript"></script>
	    <script src="<?php echo js_url('jquery.toastmessage'); ?>" type="text/javascript"></script>

		<?php if(isset($allPartenaires) && $allPartenaires != null)
		{ ?>
			<script>
				$(function() {
					<?php
					$data = array();
					foreach($allPartenaires as $all):
						$data[] = $all->structure;
					endforeach;
					$list = json_encode($data); ?>
					var availableTags = <?=$list?>;
					$("#structure, #structure").autocomplete({
						source: availableTags
					});
				});
			</script>
		<?php } ?>

	</head>
    
	<body id="public">

		
		<?php if(isset($alreadyCreated))
		{
			echo "<script> $().toastmessage('showErrorToast', \"Partenaire déjà ajouté !\");</script>";
		} ?>

		<table>
			<caption>Partenaires Initiaux</caption>
			<thead>
				<tr>
					<th>Structure</th>
					<th>Type</th>
					<th>Nom</th>
					<th>Qualité</th>
					<th>Coordonnées</th>
				</tr>
			</thead> 
			<?php if(isset($partenairesInitiaux) && $partenairesInitiaux != null)
					{
						foreach($partenairesInitiaux as $partenairesInit): ?>		
							<tr>
								<td><?php echo htmlentities($partenairesInit->structure); ?></td>
								<td><?php echo $partenairesInit->type; ?></td>
								<td><?php echo $partenairesInit->nom; ?></td>
								<td><?php echo $partenairesInit->qualite ?></td>
								<td><?php echo $partenairesInit->coordonnees ?></td>
							</tr>
						<?php endforeach; } ?>
						<form id="initForm" method="post" action="<?php echo site_url(); ?>partenaires/insertionPartenaire" autocomplete="off">
							<tr>
								<td><input id="structure" name="structure" type="text" required /></td>
								<td><select id="type" name="type" required />
										<option value="" >	
										</option>
										<option value="Soins" >
											Soins
										</option>
										<option value="Médico-social" >
											Médico-social
										</option>
										<option value="Social (CG)" >
											Social (CG)
										</option>
										<option value="Scolaire" >
											Scolaire 
										</option>
										<option value="Autres" >
											Autres
										</option>
										</select>
								</td>
								<td><input id="nom" name="nom" type="text" required /></td>
								<td><input id="qualite" name="qualite" type="text" required /></td>
								<td><input id="coordonnees" name="coordonnees" type="text" required /></td>
							</tr>
							<input type="submit" name="submitInit" class="hide-submit" value="Envoyer"> 
						</form>
		</table>
		<br /><br /><br />
		<table>
		<caption>Partenaires Impliqués</caption>
		<thead>
			<tr>
				<th>Structure</th>
				<th>Type</th>
				<th>Nom</th>
				<th>Qualité</th>
				<th>Coordonnées</th>
			</tr>
		</thead> 
		<?php if(isset($partenairesImpliques) && $partenairesImpliques != null)
				{
					foreach($partenairesImpliques as $partenairesImpli): ?>		
						<tr>
							<td><?php echo htmlentities($partenairesImpli->structure); ?></td>
							<td><?php echo $partenairesImpli->type; ?></td>
							<td><?php echo $partenairesImpli->nom; ?></td>
							<td><?php echo $partenairesImpli->qualite ?></td>
							<td><?php echo $partenairesImpli->coordonnees ?></td>
						</tr>
					<?php endforeach; } ?>
					<form id="impliForm" method="post" action="<?php echo site_url(); ?>partenaires/insertionPartenaire" autocomplete="off">
						<tr>
							<td><input id="structure" name="structure" type="text" required /></td>
							<td><select id="type" name="type" required />
									<option value="" >	
									</option>
									<option value="Soins" >
										Soins
									</option>
									<option value="Médico-social" >
										Médico-social
									</option>
									<option value="Social (CG)" >
										Social (CG)
									</option>
									<option value="Scolaire" >
										Scolaire 
									</option>
									<option value="Autres" >
										Autres
									</option>
									</select>
							</td>
							<td><input id="nom" name="nom" type="text" required /></td>
							<td><input id="qualite" name="qualite" type="text" required /></td>
							<td><input id="coordonnees" name="coordonnees" type="text" required /></td>
						</tr>
						<input type="submit" name="submitImpli" class="hide-submit" value="Envoyer"> 
					</form>
		</table>
		
	</body>
</html>