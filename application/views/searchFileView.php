<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->config->item('charset'); ?>" />
		<link rel="icon" type="image/ico" href="<?php echo img_url('Logo_DROITE.ico'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('structure'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('form'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('table'); ?>" />
	</head>
    
	<body id="public">
	<div id="container" class="ltr">

	<?php if(isset($patients) && $patients != null)
	{	?>
		<table>
			<thead>
				<tr>
					<th>Nom du Patient</th>
					<th>Prénom du Patient</th>
					<th>Numéro de Téléphone</th>
					<th>Numéro de Dossier</th>
					<th>Dossier du Patient</th>
				</tr>
			</thead>	<?php
		foreach($patients as $patient): ?>		
			<tr>
				<td><?php echo htmlentities($patient->nomPatient); ?></td>
				<td><?php echo $patient->prenomPatient; ?></td>
				<td><?php echo $patient->numero; ?></td>
				<td><?php echo $patient->idPatient . '-' . $patient->annee; ?></td>
				<td><form method="post" action="<?php echo site_url(); ?>ouvertureDossier/dossierPatient">
						<input type="hidden" name="idPatient"  value="<?php echo $patient->idPatient; ?>">
						<input border=0 name="searchSubmit" width="32" height="32" src="<?php echo img_url('Documents.ico'); ?>" type="image" Value="submit" align="middle" /> 
					</form></td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php }	?>

	</div><!--container-->
	</body>

</html>