<!DOCTYPE html>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo css_url('style'); ?>" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/ico" href="<?php echo img_url('Logo_DROITE.ico'); ?>" />
<script src="<?php echo js_url('cufon-yui'); ?>" type="text/javascript"></script>
<script src="<?php echo js_url('cufon-replace'); ?>" type="text/javascript"></script>
<script src="<?php echo js_url('Gill_Sans_400.font'); ?>" type="text/javascript"></script>
</head>
<body id="page1">
<div class="tail-top">
	<div class="tail-bottom">
		<div id="main">
<!-- HEADER -->
			<?php include("header.php"); ?>
			<div class="row-2">
				<ul>
					<li><?php echo anchor('', 'Accueil'); ?></li>
					<li><?php echo anchor('creationDossier', 'Créer un Dossier'); ?></li>
					<li><?php echo anchor('ouvertureDossier', 'Ouvrir un Dossier', array('class' => 'active')); ?></li>
					<li><?php echo anchor('requetes', 'Requêtes'); ?></li>
				</ul>
			</div>
		</div>
<!-- CONTENT -->
			<div id="content">
				<div id="slogan">
					<div class="inside">
						<?php echo 'Dossier Patient n°'. $_SESSION['idPatient'] . '-' . $row->annee . ' : ' . $row->nomPatient . ' ' . $row->prenomPatient; ?><br /><br /><br />
						<ul>
							<li><h4><span><?php echo anchor('signaletique', 'Accéder à la signalétique', array('class' => 'fontPartenaires')); ?></span></h4></li>
							<li><h4><span><?php echo anchor('parcours', 'Accéder au parcours', array('class' => 'fontPartenaires')); ?></span></h4></li>
							<li><h4><span><?php echo anchor('partenaires', 'Accéder aux partenaires', array('class' => 'fontPartenaires')); ?></span></h4></li>
							<li><h4><span><?php echo anchor('complements', 'Accéder aux compléments', array('class' => 'fontPartenaires')); ?></span></h4></li>
							<li><h4><span><?php echo anchor('actions', 'Accéder aux actions', array('class' => 'fontPartenaires')); ?></span></h4></li>
						</ul>
					</div>
				</div>
				<div class="content">
			</div>
		</div>
	</div>
</div>
</body>
</html>