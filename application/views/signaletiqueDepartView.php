<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->config->item('charset'); ?>" />
		<link rel="icon" type="image/ico" href="<?php echo img_url('Logo_DROITE.ico'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('structure'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('form'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('jquery.toastmessage'); ?>" />
		<script src="<?php echo js_url('jquery.min'); ?>" type="text/javascript"></script>
	    <script src="<?php echo js_url('jquery.toastmessage'); ?>" type="text/javascript"></script>
	</head>
    
	<body id="public">
	<div id="container" class="ltr">

	<?php if(isset($alreadyCreated))
	{
		echo "<script> $().toastmessage('showErrorToast', \"Patient déjà ajouté !\");</script>";
	} ?>

	<form id="FormContact" class="wufoo topLabel page" accept-charset="UTF-8" enctype="multipart/form-data" 
	method="post" action="<?php echo site_url(); ?>creationDossier/ajoutPatient" autocomplete="off">

	<header id="header" class="info">
		<h2>Signalétique</h2>
	</header>

	<ul>
		<li id="foli1" class="notranslate leftHalf">
			<label class="desc" id="title1" for="Field1-1">
				Patient
			</label>
			<span>
				<input id="Field1-2" name="Field1-2" type="text" class="field text ln" size="14" value="<?php if($this->input->post('Field1-2')) { echo htmlentities($this->input->post('Field1-2'));}?>" required />
				<label for="Field1-2">Nom</label>
			</span>
			<span>
				<input id="Field1-1" name="Field1-1" type="text" class="field text fn" size="14" value="<?php if($this->input->post('Field1-1')) { echo htmlentities($this->input->post('Field1-1'));}?>" required />
				<label for="Field1-1">Prénom</label>
			</span>
		</li>
		<li id="foli2" class="notranslate rightHalf">
			<span id="ChildOrNot">
				<input id="Field2-1" name="Field2-1" type="radio" value="Adulte" required />Adulte
				<input id="Field2-2" name="Field2-1" type="radio" value="Enfant" required />Enfant
			</span>
		</li>
		<li id="foli3" class="notranslate leftHalf">
			<label class="desc" id="title3" for="Field3">
				Sexe
			</label>
			<div>
				<select id="Field3" name="Field3" class="field select" required >
				<option value="" >	
				</option>
				<option value="Masculin" >
					Masculin
				</option>
				<option value="Féminin" >
					Féminin
				</option>
				</select>
			</div>
		</li>
		<li id="foli4" class="date notranslate rightHalf">
			<label class="desc" id="title4" for="Field4-1">
				Date de naissance
			</label>
			<span>
				<input id="Field4-1" name="Field4-1" type="text" class="field text" maxlength="2" size="2" value="<?php if($this->input->post('Field4-1')) { echo htmlentities($this->input->post('Field4-1'));}?>" required />
				<label for="Field4-1">JJ</label>
			</span> 
				<span class="symbol">/</span>
			<span>
				<input id="Field4-2" name="Field4-2" type="text" class="field text" maxlength="2" size="2" value="<?php if($this->input->post('Field4-2')) { echo htmlentities($this->input->post('Field4-2'));}?>" required />
				<label for="Field4-2">MM</label>
			</span>
				<span class="symbol">/</span>
			<span>
				<input id="Field4-3" name="Field4-3" type="text" class="field text" maxlength="4" size="4" value="<?php if($this->input->post('Field4-3')) { echo htmlentities($this->input->post('Field4-3'));}?>" required />
				<label for="Field4-3">AAAA</label>
			</span>
		</li>
		<li id="foli5" class="phone notranslate leftHalf">
			<label class="desc" id="title5" for="Field5">
				Dépt Résidence
			</label>
				<span>
					<input id="Field5" name="Field5" type="text" class="field text addr" maxlength="2" value="<?php if($this->input->post('Field5')) { echo htmlentities($this->input->post('Field5'));}?>" required />
				</span>
		</li>
		<li id="foli6" class="notranslate leftHalf">
			<label class="desc" id="title6" for="Field6">
				N° de Téléphone
			</label>
			<div>
				<input id="Field6" name="Field6" type="tel" class="field text" maxlength="10" value="<?php if($this->input->post('Field6')) { echo htmlentities($this->input->post('Field6'));}?>" />
			</div>
		</li>
		<li id="foli7" class="notranslate rightHalf">
			<label class="desc" id="title7" for="Field7">
				Connaissance du Réseau VADLR ?
			</label>
			<div>
				<select id="Field7" name="Field7" class="field select med" required >
				<option value="" >	
				</option>
				<option value="Annuaire" >
					Annuaire
				</option>
				<option value="Réseaux Sociaux" >
					Réseaux Sociaux
				</option>
				<option value="Forum" >
					Forum
				</option>
				<option value="Travail" >
					Travail
				</option>
				<option value="Association" >
					Association
				</option>
				<option value="Moteur de Recherche" >
					Moteur de Recherche
				</option>
				<option value="Bouche à Oreille" >
					Bouche à Oreille
				</option>
				<option value="Autre" >
					Autre
				</option>
				</select>
			</div>
		</li>

		<li class="buttons ">
			<div>
				<input id="saveForm" name="saveForm" class="btTxt submit" type="submit" value="Confirmer"/>
			</div>
		</li>

	</ul>
	</form> 

	</div><!--container-->
	</body>

</html>