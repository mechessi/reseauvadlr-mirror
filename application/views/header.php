<style type="text/css">

#recherche
{
	border:none;
	color:#888888;
	background:url("<?php echo img_url('searchBox.png'); ?>") no-repeat;
	font-family:Arial,Helvetica,sans-serif;
	font-size:15px;
	height:36px;
	line-height:20px;
	outline:medium none;
	padding:0 0 0 35px;
	text-shadow:1px 1px 0 white;
	width:200px;
}

</style> 


<div id="header">
	<div class="row-1">
		<div class="fleft"><?php echo anchor('', 'Réseau <span>VADLR</span>'); ?></div>
			<ul>
				<li>
					<form id="searchForm" method="get" action="<?php echo site_url(); ?>ouvertureDossier/recherchePatient" autocomplete="off">
						<p>
							<input id="recherche" name="recherche" type="text" placeholder="Recherche..." />
						</p>
						<input type="submit" class="hide-submit" value="Rechercher"> 
					</form>
				</li>
			</ul>
	</div>