<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->config->item('charset'); ?>" />
		<link rel="icon" type="image/ico" href="<?php echo img_url('Logo_DROITE.ico'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('structure'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo css_url('form'); ?>" />
		<style type="text/css">
			#container
			{
				margin:0 auto 10px auto;
				width:700px;
			}
			.c1
			{
				text-align:center;
				border: none;
				display: block;
				width: 70px;
				float: left;
			}
			 
			.c2, .c3
			{
				text-align:center;
				border: none;
			}
			 
			table.ex1 tr:first-child td
			{
				border:none;
			}
			 
			table
			{
				border: none;
				border-collapse:collapse;
			}
		</style>
	</head>
    
	<body id="public">
	<div id="container" class="ltr">

	<form id="FormContact" class="wufoo topLabel page" accept-charset="UTF-8" enctype="multipart/form-data" 
	method="post" action="<?php echo site_url(); ?>signaletique/ajoutInformations" autocomplete="off">

	<ul>
		<li id="foli1" class="notranslate leftHalf">
			<div class="desc">
				Patient
			</div>
			<div>
				<?php if(!empty($row)) echo trim($row->nomPatient . ' ' . $row->prenomPatient); ?>
			</div>
		</li>
		<li id="foli8" class="notranslate rightHalf">
			<div class="desc">
				Sexe
			</div>
			<div>
				<?php if(!empty($row)) echo trim($row->sexePatient); ?>
			</div>
		</li>
		<li id="foli9" class="date notranslate leftHalf">
			<div class="desc">
				Date de naissance
			</div>
			<div>
				<?php if(!empty($row)) echo date("d-m-Y", strtotime($row->dateNaissance)); ?>
			</div>
		</li>
			<?php if(!empty($row) && !empty($row->pathologie))
			{ ?>
				<li id="foli10" class="notranslate rightHalf">
					<div class="desc">
						Pathologie
					</div>
					<div>
						<?php echo trim($row->pathologie); ?>
					</div>
				</li>
	  <?php }
			else
			{ ?>
				<li id="foli11" class="notranslate rightHalf">
					<label class="desc" id="title11" for="Field1">
						Pathologie
					</label>
					<div>
						<input id="Field1" name="Field1" type="text" class="field text" />
					</div>
				</li>
	  <?php } ?>
	  <li id="foli2" class="notranslate leftHalf">
			<div class="desc">
				État
			</div>
			<div>
				<?php if(!empty($row)) echo trim($row->typePatient); ?>
			</div>
		</li>
	  <?php if(!empty($row) && !empty($row->representant))
			{	?>
				<li id="foli3" class="notranslate rightHalf">
					<div class="desc">
						Représentant Légal
					</div>
					<div>
						<?php echo trim($row->representant); ?>
					</div>
				</li>
	  <?php }
			else if (!empty($row) && $row->typePatient == 'Adulte')
			{	?>
				<li id="foli4" class="notranslate rightHalf">
					<label class="desc" id="title4" for="Field2">
						Représentant Légal
					</label>
					<div>
						<input id="Field2" name="Field2" type="text" class="field text large" />
					</div>
				</li>
	  <?php }
			else if (!empty($row) && $row->typePatient == 'Enfant')
			{	?>
				<li id="foli5" class="notranslate rightHalf">
					<label class="desc" id="title5" for="Field3">
						Représentant Légal
					</label>
					<div>
						<select id="Field3" name="Field3" class="field select med" >
							<option value="" >	
							</option>
							<option value="Père" >
								Père
							</option>
							<option value="Mère" >
								Mère
							</option>
							<option value="Père-Mère" >
								Père-Mère
							</option>
						</select>
					</div>
				</li>
	  <?php } 
	  		if(!empty($row) && !empty($row->domicileAdulte) && $row->typePatient == 'Adulte')
			{	?>
				<li id="foli6" class="notranslate leftHalf">
					<div class="desc">
						Domicile Adulte
					</div>
					<div>
						<?php echo trim($row->domicileAdulte); ?>
					</div>
				</li>
	  <?php }
			else if(!empty($row) && empty($row->domicileAdulte) && $row->typePatient == 'Adulte')
			{	?>
				<li id="foli7" class="notranslate leftHalf">
					<label class="desc" id="title7" for="Field4">
						Domicile Adulte
					</label>
					<div>
						<select id="Field4" name="Field4" class="field select" >
							<option value="" >	
							</option>
							<option value="Autonome" >
								Autonome
							</option>
							<option value="ESMS" >
								ESMS
							</option>
							<option value="Hébergé(parents, etc…)" >
								Hébergé(parents, etc…)
							</option>
						</select>
					</div>
				</li>
	  <?php } ?>
		<li id="foli15" class="complex notranslate">
			<label class="desc" id="title15" for="Field6">
				Adresse
			</label>
			<div>
				<span class="full addr1">
					<input id="Field6" name="Field6" type="text" class="field text addr" value="<?php if(!empty($row->rue)) echo trim($row->rue); ?>" />
					<label for="Field6">Rue</label>
				</span>
				<span class="left city">
					<input id="Field7" name="Field7" type="text" class="field text addr" value="<?php if(!empty($row->codepostal)) echo trim($row->codepostal); ?>" />
					<label for="Field7">Code Postal</label>
				</span>
				<span class="right state">
					<input id="Field8" name="Field8" type="text" class="field text addr" value="<?php if(!empty($row->ville)) echo trim($row->ville); ?>" />
					<label for="Field8">Ville</label>
				</span>
			</div>
		</li>
		<li id="foli13" class="notranslate leftHalf">
			<label class="desc" id="title13" for="Field5">
				N° de Téléphone
			</label>
			<div>
				<input id="Field5" name="Field5" type="text" class="field text" value="<?php if(!empty($row->numero)) echo trim($row->numero); ?>" />
			</div>
		</li>
		<li id="foli16" class="notranslate section">
			<section>
				<h3 id="title16">
					Parents
				</h3>
			</section>
		</li>
		<?php if(!empty($row) && !empty($row->situation))
			{	?>
				<li id="foli18" class="notranslate leftHalf">
					<div class="desc">
						Situation de la Famille
					</div>
					<div>
						<?php echo trim($row->situation); ?>
					</div>
				</li>
	  <?php }
			else
			{	?>
				<li id="foli19" class="notranslate leftHalf">
					<label class="desc" id="title21" for="Field9">
						Situation de la Famille
					</label>
					<div>
						<select id="Field9" name="Field9" class="field select med" >
							<option value="" >	
							</option>
							<option value="En Couple" >
								En Couple
							</option>
							<option value="Séparés" >
								Séparés
							</option>
							<option value="Parent Isolé" >
								Parent Isolé
							</option>
						</select>
					</div>
				</li>
	  <?php } ?>
	  	<li id="foli32" class="notranslate rightHalf">
		<div id="FragSociale">
			Fragilité sociale ? <input TYPE="checkbox" NAME="fragSociale" value="oui" <?php if(!empty($row->fragSociale) && $row->fragSociale == 'oui') { ?> checked <?php } ?> >
		</div>
		</li>
		<li id="foli17" class="notranslate leftHalf">
			<table class="ex1" border="1">
				<tr>
					<td class="c1"></td>
					<td class="c2">Mère</td>
					<td class="c3">Père</td>
				</tr>
				 
				<tr>
					<td class="c1">Nom : </td>
					<td class="c2"><input id="Field10" name="Field10" type="text" class="field text" value="<?php if(!empty($row->nomMere)) echo $row->nomMere; ?>" /></td>
					<td class="c3"><input id="Field11" name="Field11" type="text" class="field text" value="<?php if(!empty($row->nomPere)) echo $row->nomPere; ?>" /></td>
				</tr>
			</table>
			<br />
				 
			<table class="ex2"  border="1">
				<tr>
					<td class="c1">Adresse : </td>
					<td class="c2"><textarea id="Field12" name="Field12" spellcheck="true" rows="5" cols="15" ><?php if(!empty($row->adrMere)) echo trim($row->adrMere); ?></textarea></td>
					<td class="c3"><textarea id="Field13" name="Field13" spellcheck="true" rows="5" cols="15" ><?php if(!empty($row->adrPere)) echo trim($row->adrPere); ?></textarea></td>
				</tr>
			</table>
			<br />
				 
			<table class="ex3"  border="1">
				<tr>
					<td  class="c1">Tél : </td>
					<td class="c2"><input id="Field14" name="Field14" type="tel" class="field text" value="<?php if(!empty($row->telMere)) echo $row->telMere; ?>" /></td>
					<td class="c3"><input id="Field15" name="Field15" type="tel" class="field text" value="<?php if(!empty($row->telPere)) echo $row->telPere; ?>" /></td>
				</tr>
				<tr>
					<td  class="c1">Email : </td>
					<td class="c2"><input id="Field16" name="Field16" type="email" class="field text" value="<?php if(!empty($row->mailMere)) echo $row->mailMere; ?>" /></td>
					<td class="c3"><input id="Field17" name="Field17" type="email" class="field text" value="<?php if(!empty($row->mailPere)) echo $row->mailPere; ?>" /></td>
				</tr>
				<tr>
					<td  class="c1">Situation : </td>
					<td class="c2">
						<?php
	  					if(!empty($row) && !empty($row->situationMere))
						{	
							 echo $row->situationMere;
						}
						else
						{ ?>
							<select id="Field18" name="Field18" class="field select large" >
							<option value="" >	
							</option>
							<option value="En Activité" >
								En Activité
							</option>
							<option value="Activité + Congé Parental" >
								Activité + Congé Parental
							</option>
							<option value="Au Chômage" >
								Au Chômage
							</option>
							<option value="Au Foyer Pour PC" >
								Au Foyer Pour PC
							</option>
							<option value="Retraité" >
								Retraité
							</option>
							<option value="AAH" >
								AAH
							</option>
							<option value="Non Renseigné" >
								Non Renseigné
							</option>
						</select>
				  <?php } ?>
					</td>
					<td class="c3">
					<?php
	  					if(!empty($row) && !empty($row->situationPere))
						{	
							 echo $row->situationPere;
						}
						else
						{ ?>
							<select id="Field19" name="Field19" class="field select large" >
							<option value="" >	
							</option>
							<option value="En Activité" >
								En Activité
							</option>
							<option value="Activité + Congé Parental" >
								Activité + Congé Parental
							</option>
							<option value="Au Chômage" >
								Au Chômage
							</option>
							<option value="Au Foyer Pour PC" >
								Au Foyer Pour PC
							</option>
							<option value="Retraité" >
								Retraité
							</option>
							<option value="AAH" >
								AAH
							</option>
							<option value="Non Renseigné" >
								Non Renseigné
							</option>
						</select>
				  <?php } ?>
					</td>
				</tr>
			</table>
		</li>
	  	<?php
	  		if(!empty($row) && !empty($row->fratrie))
			{	?>
				<li id="foli24" class="notranslate leftHalf">
					<div class="desc">
						Fratrie
					</div>
					<div>
						<?php echo trim($row->fratrie); ?>
					</div>
				</li>
	  <?php }
			else
			{	?>
				<li id="foli25" class="notranslate leftHalf">
					<label class="desc" id="title25" for="Field20">
						Fratrie
					</label>
					<div>
						<input id="Field20" name="Field20" type="text" class="field text large" />
						<label for="Field20">Frères et Soeurs</label>
					</div>
				</li>
	  <?php } ?>
	  <li id="foli27" class="notranslate section">
			<section>
				<h3 id="title27">
					Dossier MDPH pour la personne concernée
				</h3>
			</section>
		</li>
		<li id="foli28" class="notranslate leftHalf">
			<div id="mdph">
				<br />AAH <input TYPE="checkbox" NAME="AAH" value="oui" <?php if(!empty($row->AAH) && $row->AAH == 'oui') { ?> checked <?php } ?> >
				<br /><br />
				RQTH <input TYPE="checkbox" NAME="RQTH" value="oui" <?php if(!empty($row->RQTH) && $row->RQTH == 'oui') { ?> checked <?php } ?> >
				<br /><br />
				AEEH <input TYPE="checkbox" NAME="AEEH" value="oui" <?php if(!empty($row->AEEH) && $row->AEEH == 'oui') { ?> checked <?php } ?> >
				<br /><br />
				PCH <input TYPE="checkbox" NAME="PCH" value="oui" <?php if(!empty($row->PCH) && $row->PCH == 'oui') { ?> checked <?php } ?> >
				<br /><br />
				CARTES <input TYPE="checkbox" NAME="CARTES" value="oui" <?php if(!empty($row->CARTES) && $row->CARTES == 'oui') { ?> checked <?php } ?> >
				<br /><br />
				Orientation ESMS <input TYPE="checkbox" NAME="ESMS" value="oui" <?php if(!empty($row->ESMS) && $row->ESMS == 'oui') { ?> checked <?php } ?> >
			</div>
		</li>
		<li id="foli30" class="notranslate rightHalf">
			<div>
				Complément :<input id="Field21" name="Field21" class="text" type="text" value="<?php if(!empty($row->complement)) echo $row->complement; ?>" />
				<br />
				Type :<input id="Field22" name="Field22" class="text" type="text" value="<?php if(!empty($row->type1)) echo $row->type1; ?>" />
				<br />
				Type :<input id="Field23" name="Field23" class="text" type="text" value="<?php if(!empty($row->type2)) echo $row->type2; ?>" />
				<br />
				Type :<input id="Field24" name="Field24" class="text" type="text" value="<?php if(!empty($row->type3)) echo $row->type3; ?>" />
			</div>
		</li>
		<li id="foli33" class="notranslate rightHalf">
			<label class="desc" id="title31" for="Field25">
				Échéance
			</label>
			<div>
				<input id="Field25" name="Field25" type="date" value="<?php if(!empty($row->echeanceAAH)) echo $row->echeanceAAH; ?>" />
				<br />
				<input id="Field26" name="Field26" type="date" value="<?php if(!empty($row->echeanceRQTH)) echo $row->echeanceRQTH; ?>" />
				<br />
				<input id="Field27" name="Field27" type="date" value="<?php if(!empty($row->echeanceAEEH)) echo $row->echeanceAEEH; ?>" />
				<br />
				<input id="Field28" name="Field28" type="date" value="<?php if(!empty($row->echeancePCH)) echo $row->echeancePCH; ?>" />
				<br />
				<input id="Field29" name="Field29" type="date" value="<?php if(!empty($row->echeanceCARTES)) echo $row->echeanceCARTES; ?>" />
				<br />
				<input id="Field30" name="Field30" type="date" value="<?php if(!empty($row->echeanceESMS)) echo $row->echeanceESMS; ?>" />
			</div>
		</li>
		<li id="foli34" class="notranslate rightHalf">
			<label class="desc" id="title34" for="Field31">
				En Cours
			</label>
			<div id="enCours">
				<input id="Field31" name="Field31" type="checkbox" value="oui" <?php if(!empty($row->coursAAH) && $row->coursAAH == 'oui') { ?> checked <?php } ?> />
				<br />
				<input id="Field32" name="Field32" type="checkbox" value="oui" <?php if(!empty($row->coursRQTH) && $row->coursRQTH == 'oui') { ?> checked <?php } ?> />
				<br />
				<input id="Field33" name="Field33" type="checkbox" value="oui" <?php if(!empty($row->coursAEEH) && $row->coursAEEH == 'oui') { ?> checked <?php } ?> />
				<br />
				<input id="Field34" name="Field34" type="checkbox" value="oui" <?php if(!empty($row->coursPCH) && $row->coursPCH == 'oui') { ?> checked <?php } ?> />
				<br />
				<input id="Field35" name="Field35" type="checkbox" value="oui" <?php if(!empty($row->coursCARTES) && $row->coursCARTES == 'oui') { ?> checked <?php } ?> />
				<br />
				<input id="Field36" name="Field36" type="checkbox" value="oui" <?php if(!empty($row->coursESMS) && $row->coursESMS == 'oui') { ?> checked <?php } ?> />
			</div>
		</li>
	<span>
		 <?php if(!empty($row) && !empty($row->refus))
		{	?>
			<div class="desc">
				Si refus, préciser :
			</div>
			<?php echo trim($row->refus);
		}
		else
		{ ?>
			<li id="foli29" class="notranslate leftHalf">
				<label class="desc" id="title29" for="Field37">
					Si refus, préciser :
				</label>
				<div>
					<input id="Field37" name="Field37" type="text" class="field text large" />
				</div>
			</li>
  <?php } ?>

		<li class="buttons ">
			<div>
				<input id="saveSignaletique" name="saveSignaletique" class="btTxt submit" type="submit" value="Confirmer"/>
				<!-- <input id="suivSignaletique" name="suivSignaletique" class="btTxt submit" type="submit" value="Suivant"/> -->
			</div>
		</li>
</span>
	</ul>
	</form> 

	</div><!--container-->
	</body>

</html>