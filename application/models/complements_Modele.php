<?php

class Complements_Modele extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getAllComplements($idPatient)
	{
		if(isset($idPatient) && !empty($idPatient))
		{
			$query = $this->db->query('SELECT * FROM complements WHERE idPatient = "' . $idPatient . '"');

			return $query->row();
		}
	}


	public function insertComplements($idPatient, $elements, $problemes, $contenu)
	{
		if(isset($idPatient) && !empty($idPatient) && isset($elements) && isset($problemes) && isset($contenu))
		{
			$query = $this->db->query('SELECT * FROM complements WHERE idPatient = "' . $idPatient . '"');

			if(count($query->result()) == 0)
			{
				$this->db->set('idPatient', $idPatient)
				->insert('complements');
			}

			$data = array(
	               'elements' => $elements,
	               'problemes' => $problemes,
	               'contenu' => $contenu
	            );

			$this->db->where('idPatient', $idPatient);
			$this->db->update('complements', $data);
		}
	}

}