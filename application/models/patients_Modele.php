<?php

class Patients_Modele extends CI_Model
{
	private $table = 'dossierpatient';

	public function __construct()
	{
		parent::__construct();
	}
	
	public function ajouter_patient($prenom, $nom, $type, $sexe, $jour, $mois, $annee, $dpt, $numero, $connaissance)
	{
		if(isset($prenom) && isset($nom) && isset($type) && isset($sexe) && isset($jour) && isset($mois) && 
			isset($annee) && isset($dpt) && isset($numero) && isset($connaissance)) 
		{
			if(!empty($prenom) && !empty($nom) && !empty($type) && !empty($sexe) && !empty($jour) && !empty($mois) && 
				!empty($annee) && !empty($dpt) && !empty($numero) && !empty($connaissance)) 
			{
				$dateNaissance = $annee . '-' . $mois . '-' . $jour;

				$query = $this->db->query('SELECT * FROM dossierpatient WHERE prenomPatient = "' . $prenom . '" AND nomPatient = "' . $nom . '"
					AND typePatient = "' . $type . '" AND sexePatient = "' . $sexe . '" AND dateNaissance = "' . $dateNaissance . '"
					AND dptResidence = "' . $dpt . '" AND numero = "' . $numero . '" AND connaissance = "' . $connaissance . '"');

				$array = $query->result();

				if(count($array) == 0)
				{					
					return $this->db->set('annee',	date("Y"))
					->set('prenomPatient', $prenom)
					->set('nomPatient', $nom)
					->set('typePatient',  $type)
					->set('sexePatient', $sexe)
					->set('dateNaissance', $dateNaissance)
					->set('dptResidence', $dpt)
					->set('numero', $numero)
					->set('connaissance', $connaissance)
					->set('autorisation', 'non')
					->set('sortie', 'non')
					->insert($this->table);
				}
				else
				{
					return null;
				}
			}
		}
	}
	
	public function count()
	{
		return $this->db->count_all($this->table);
	}

	public function getAllPatientMatchWith($recherche)
	{
		if(isset($recherche))
		{
			$query = $this->db->query('SELECT * FROM dossierpatient WHERE nomPatient LIKE "%' . $recherche . '%" UNION
						SELECT * FROM dossierpatient WHERE prenomPatient LIKE "%' . $recherche . '%" UNION
						SELECT * FROM dossierpatient WHERE CONCAT(prenomPatient, nomPatient)  LIKE "%' . $recherche . '%"
						UNION
						SELECT * FROM dossierpatient WHERE CONCAT(nomPatient, prenomPatient)  LIKE "%' . $recherche . '%"');

			return $query->result();
		}
	}

	public function getPatientById($idPatient)
	{
		if(isset($idPatient) && !empty($idPatient))
		{
			$query = $this->db->query('SELECT * FROM dossierpatient WHERE idPatient = "' . $idPatient . '"');

			return $query->row();
		}
	}

	public function getLastId()
	{
		$this->db->select_max('idPatient');
		$Q = $this->db->get('dossierpatient');
		$row = $Q->row_array();
		return $row['idPatient'];
	}

	public function getAutorisation($idPatient)
	{
		if(isset($idPatient) && !empty($idPatient))
		{
			$query = $this->db->query('SELECT autorisation FROM dossierpatient WHERE idPatient = "' . $idPatient . '"');

			$row = $query->row_array();
			return $row['autorisation'];
		}
	}

	public function ajoutInfos($idPatient, $tel, $rue, $code, $ville, $nomMere, $nomPere, $adrMere, $adrPere, $telMere,
		$telPere, $mailMere, $mailPere, $complement, $type1, $type2, $type3, $echeanceAAH, $echeanceRQTH,
		$echeanceAEEH, $echeancePCH, $echeanceCARTES, $echeanceESMS, $coursAAH, $coursRQTH, $coursAEEH, $coursPCH, 
		$coursCARTES, $coursESMS, $AAH, $RQTH, $AEEH, $PCH, $CARTES, $ESMS, $fragSociale)
	{
		if(isset($idPatient) && isset($tel) && isset($rue) && isset($code) && isset($ville) && isset($nomMere) 
			&& isset($nomPere)&& isset($adrMere) && isset($adrPere) && isset($telMere) && isset($telPere) 
				&& isset($mailMere) && isset($mailPere) && isset($complement) && isset($type1) && isset($type2) 
					&& isset($type3) && isset($echeanceAAH) && isset($echeanceRQTH) && isset($echeanceAEEH) && isset($echeancePCH) 
						&& isset($echeanceCARTES) && isset($echeanceESMS) && isset($coursAAH) && isset($coursRQTH) 
							&& isset($coursAEEH) && isset($coursPCH) && isset($coursCARTES) && isset($coursESMS)
								&& isset($AAH) && isset($RQTH) && isset($AEEH) && isset($PCH) && isset($CARTES) && isset($ESMS)
									&& isset($fragSociale))
		{
			$data = array(
				'numero' => $tel, 
				'rue' => $rue, 
				'codepostal' => $code, 
				'ville' => $ville, 
				'nomMere' => $nomMere, 
				'nomPere' => $nomPere, 
				'adrMere' => $adrMere, 
				'adrPere' => $adrPere, 
				'telMere' => $telMere,
				'telPere' => $telPere, 
				'mailMere' => $mailMere, 
				'mailPere' => $mailPere, 
				'complement' => $complement, 
				'type1' => $type1, 
				'type2' => $type2, 
				'type3' => $type3, 
				'echeanceAAH' => $echeanceAAH, 
				'echeanceRQTH' => $echeanceRQTH,
				'echeanceAEEH' => $echeanceAEEH, 
				'echeancePCH' => $echeancePCH, 
				'echeanceCARTES' => $echeanceCARTES, 
				'echeanceESMS' => $echeanceESMS, 
				'coursAAH' => $coursAAH, 
				'coursRQTH' => $coursRQTH, 
				'coursAEEH' => $coursAEEH, 
				'coursPCH' => $coursPCH, 
				'coursCARTES' => $coursCARTES, 
				'coursESMS' => $coursESMS,
				'AAH' => $AAH, 
				'RQTH' => $RQTH,
				'AEEH' => $AEEH, 
				'PCH' => $PCH, 
				'CARTES' => $CARTES, 
				'ESMS' => $ESMS,
				'fragSociale' => $fragSociale
            );
			$this->db->where('idPatient', $idPatient);
			$this->db->update('dossierpatient', $data);
		}
	}

	public function setPathologie($idPatient, $pathologie)
	{
		if(isset($idPatient) && isset($pathologie) && !empty($pathologie))
		{
			$data = array(
               'pathologie' => $pathologie
            );
			$this->db->where('idPatient', $idPatient);
			$this->db->update('dossierpatient', $data);
		}
	}

	public function setRepresentant($idPatient, $representant)
	{
		if(isset($idPatient) && isset($representant) && !empty($representant))
		{
			$data = array(
               'representant' => $representant
            );
			$this->db->where('idPatient', $idPatient);
			$this->db->update('dossierpatient', $data);
		}
	}

	public function setDomicile($idPatient, $domicile)
	{
		if(isset($idPatient) && isset($domicile) && !empty($domicile))
		{
			$data = array(
               'domicileAdulte' => $domicile
            );
			$this->db->where('idPatient', $idPatient);
			$this->db->update('dossierpatient', $data);
		}
	}

	public function setSituation($idPatient, $situation)
	{
		if(isset($idPatient) && isset($situation) && !empty($situation))
		{
			$data = array(
               'situation' => $situation
            );
			$this->db->where('idPatient', $idPatient);
			$this->db->update('dossierpatient', $data);
		}
	}

	public function setSituationMere($idPatient, $situationMere)
	{
		if(isset($idPatient) && isset($situationMere) && !empty($situationMere))
		{
			$data = array(
               'situationMere' => $situationMere
            );
			$this->db->where('idPatient', $idPatient);
			$this->db->update('dossierpatient', $data);
		}
	}

	public function setSituationPere($idPatient, $situationPere)
	{
		if(isset($idPatient) && isset($situationPere) && !empty($situationPere))
		{
			$data = array(
               'situationPere' => $situationPere
            );
			$this->db->where('idPatient', $idPatient);
			$this->db->update('dossierpatient', $data);
		}
	}

	public function setFratrie($idPatient, $fratrie)
	{
		if(isset($idPatient) && isset($fratrie) && !empty($fratrie))
		{
			$data = array(
               'fratrie' => $fratrie
            );
			$this->db->where('idPatient', $idPatient);
			$this->db->update('dossierpatient', $data);
		}
	}

	public function setRefus($idPatient, $refus)
	{
		if(isset($idPatient) && isset($refus) && !empty($refus))
		{
			$data = array(
               'refus' => $refus
            );
			$this->db->where('idPatient', $idPatient);
			$this->db->update('dossierpatient', $data);
		}
	}
}