<?php

class Partenaires_Modele extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getAllPartenairesInitiaux($idPatient)
	{
		if(isset($idPatient) && !empty($idPatient))
		{
			$query = $this->db->query('SELECT * FROM partenairesinitiaux WHERE idPatient = "' . $idPatient . '" ORDER BY structure ASC');

			return $query->result();
		}
	}

	public function getAllPartenairesImpliques($idPatient)
	{
		if(isset($idPatient) && !empty($idPatient))
		{
			$query = $this->db->query('SELECT * FROM partenairesimpliques WHERE idPatient = "' . $idPatient . '" ORDER BY structure ASC');

			return $query->result();
		}
	}

	public function getAllPartenaires()
	{
		$query = $this->db->query('SELECT * FROM (SELECT * FROM partenairesimpliques UNION SELECT * FROM partenairesinitiaux) t
									GROUP BY structure');

		return $query->result();
	}

	public function ajoutPartenairesImpliques($idPatient, $structure, $type, $nom, $qualite, $coordonnees)
	{
		if(isset($idPatient) && isset($structure) && isset($type) && isset($nom) && isset($qualite) && isset($coordonnees)) 
		{
			if(!empty($idPatient) && !empty($structure) && !empty($type) && !empty($nom) && !empty($qualite) && !empty($coordonnees)) 
			{
				$query = $this->db->query('SELECT * FROM partenairesimpliques WHERE idPatient = "' . $idPatient . '" AND structure = "' . $structure . '"
					AND type = "' . $type . '" AND nom = "' . $nom . '" AND qualite = "' . $qualite . '"
					AND coordonnees = "' . $coordonnees . '"');

				$array = $query->result();

				if(count($array) == 0)
				{
					return $this->db->set('structure',	$structure)
					->set('idPatient', $idPatient)
					->set('type', $type)
					->set('nom', $nom)
					->set('qualite',  $qualite)
					->set('coordonnees', $coordonnees)
					->insert('partenairesimpliques');
				}
				else
				{
					return null;
				}
			}
		}
	}

	public function ajoutPartenairesInitiaux($idPatient, $structure, $type, $nom, $qualite, $coordonnees)
	{
		if(isset($idPatient) && isset($structure) && isset($type) && isset($nom) && isset($qualite) && isset($coordonnees)) 
		{
			if(!empty($idPatient) && !empty($structure) && !empty($type) && !empty($nom) && !empty($qualite) && !empty($coordonnees)) 
			{
				$query = $this->db->query('SELECT * FROM partenairesinitiaux WHERE idPatient = "' . $idPatient . '" AND structure = "' . $structure . '"
					AND type = "' . $type . '" AND nom = "' . $nom . '" AND qualite = "' . $qualite . '"
					AND coordonnees = "' . $coordonnees . '"');

				$array = $query->result();

				if(count($array) == 0)
				{
					return $this->db->set('structure',	$structure)
					->set('idPatient', $idPatient)
					->set('type', $type)
					->set('nom', $nom)
					->set('qualite',  $qualite)
					->set('coordonnees', $coordonnees)
					->insert('partenairesinitiaux');
				}
				else
				{
					return null;
				}
			}
		}
	}

}