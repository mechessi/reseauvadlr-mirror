-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 11 Février 2014 à 19:11
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `reseauvadlr`
--
CREATE DATABASE IF NOT EXISTS `reseauvadlr` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `reseauvadlr`;

-- --------------------------------------------------------

--
-- Structure de la table `complements`
--

CREATE TABLE IF NOT EXISTS `complements` (
  `idPatient` int(11) NOT NULL,
  `elements` text NOT NULL,
  `problemes` text NOT NULL,
  `contenu` text NOT NULL,
  PRIMARY KEY (`idPatient`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `complements`
--

INSERT INTO `complements` (`idPatient`, `elements`, `problemes`, `contenu`) VALUES
(1, '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `dossierpatient`
--

CREATE TABLE IF NOT EXISTS `dossierpatient` (
  `idPatient` int(11) NOT NULL AUTO_INCREMENT,
  `annee` varchar(4) NOT NULL,
  `nomPatient` varchar(30) NOT NULL,
  `prenomPatient` varchar(30) NOT NULL,
  `typePatient` varchar(30) NOT NULL,
  `sexePatient` varchar(30) NOT NULL,
  `dateNaissance` date NOT NULL,
  `dptResidence` varchar(2) NOT NULL,
  `numero` varchar(50) NOT NULL,
  `connaissance` varchar(30) NOT NULL,
  `autorisation` varchar(10) NOT NULL,
  `pathologie` varchar(100) NOT NULL,
  `representant` varchar(100) NOT NULL,
  `domicileAdulte` varchar(100) NOT NULL,
  `rue` varchar(100) NOT NULL,
  `codepostal` varchar(100) NOT NULL,
  `ville` varchar(100) NOT NULL,
  `situation` varchar(100) NOT NULL,
  `fragSociale` varchar(100) NOT NULL,
  `nomMere` varchar(100) NOT NULL,
  `nomPere` varchar(100) NOT NULL,
  `adrMere` varchar(100) NOT NULL,
  `adrPere` varchar(100) NOT NULL,
  `telMere` varchar(100) NOT NULL,
  `telPere` varchar(100) NOT NULL,
  `mailMere` varchar(100) NOT NULL,
  `mailPere` varchar(100) NOT NULL,
  `situationMere` varchar(100) NOT NULL,
  `situationPere` varchar(100) NOT NULL,
  `fratrie` varchar(100) NOT NULL,
  `AAH` varchar(100) NOT NULL,
  `RQTH` varchar(100) NOT NULL,
  `AEEH` varchar(100) NOT NULL,
  `PCH` varchar(100) NOT NULL,
  `CARTES` varchar(100) NOT NULL,
  `ESMS` varchar(100) NOT NULL,
  `complement` varchar(100) NOT NULL,
  `type1` varchar(100) NOT NULL,
  `type2` varchar(100) NOT NULL,
  `type3` varchar(100) NOT NULL,
  `echeanceAAH` varchar(100) NOT NULL,
  `echeanceRQTH` varchar(100) NOT NULL,
  `echeanceAEEH` varchar(100) NOT NULL,
  `echeancePCH` varchar(100) NOT NULL,
  `echeanceCARTES` varchar(100) NOT NULL,
  `echeanceESMS` varchar(100) NOT NULL,
  `coursAAH` varchar(100) NOT NULL,
  `coursRQTH` varchar(100) NOT NULL,
  `coursAEEH` varchar(100) NOT NULL,
  `coursPCH` varchar(100) NOT NULL,
  `coursCARTES` varchar(100) NOT NULL,
  `coursESMS` varchar(100) NOT NULL,
  `refus` varchar(100) NOT NULL,
  `sortie` varchar(100) NOT NULL,
  PRIMARY KEY (`idPatient`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Contenu de la table `dossierpatient`
--

INSERT INTO `dossierpatient` (`idPatient`, `annee`, `nomPatient`, `prenomPatient`, `typePatient`, `sexePatient`, `dateNaissance`, `dptResidence`, `numero`, `connaissance`, `autorisation`, `pathologie`, `representant`, `domicileAdulte`, `rue`, `codepostal`, `ville`, `nomMere`, `nomPere`, `sortie`, `situationMere`, `situationPere`, `situation`, `fragSociale`, `adrMere`, `adrPere`, `telMere`, `telPere`, `mailMere`, `mailPere`, `fratrie`, `AAH`, `RQTH`, `AEEH`, `PCH`, `CARTES`, `ESMS`, `complement`, `type1`, `type2`, `type3`, `echeanceAAH`, `echeanceRQTH`, `echeanceAEEH`, `echeancePCH`, `echeanceCARTES`, `echeanceESMS`, `coursAAH`, `coursRQTH`, `coursAEEH`, `coursPCH`, `coursCARTES`, `coursESMS`, `refus`) VALUES
(1, '2014', 'bon', 'jean', 'Adulte', 'Masculin', '0000-00-00', '34', '0634958675', 'Réseaux Sociaux', 'non', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, '2014', 'my', 'sam', 'Enfant', 'Masculin', '0000-00-00', '34', '0254256589', 'Annuaire', '', 'autisme', '', 'Autonome', '', '11000', '', 'Michelle', 'm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, '2014', 'B', 'Jean', 'Adulte', 'Masculin', '0000-00-00', 'zq', 'zqdqzdqzdq', 'Travail', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, '2014', 'T', 'BENJAMN', 'Adulte', 'Féminin', '0000-00-00', 'ZQ', 'QZDQZQZQDQ', 'Travail', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(23, '2014', 'a', 'a', 'Adulte', 'Masculin', '0000-00-00', 'a', 'a', 'Annuaire', 'non', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(24, '2014', 'Montrand', 'Jean-Charles', 'Adulte', 'Masculin', '0000-00-00', 'a', 'a', 'Annuaire', 'non', '', '', '', '', '', '', '', '', '', 'Activité + Congé Parental', 'En Activité', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(25, '2014', 'c', 'c', 'Adulte', 'Masculin', '0000-00-00', 'c', 'c', 'Réseaux Sociaux', 'non', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(26, '2014', 'k', 'k', 'Adulte', 'Féminin', '0000-00-00', 'k', 'k', 'Annuaire', 'non', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(27, '2014', 'v', 'v', 'Adulte', 'Féminin', '1994-12-02', 'v', 'v', 'Forum', 'non', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(28, '2014', 'cc', 'c', 'Adulte', 'Masculin', '0000-00-00', 'c', 'c', 'Annuaire', 'non', '', '', '', '', '', '', '', '', 'non', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(29, '2014', 'c', 'cc', 'Adulte', 'Masculin', '0000-00-00', 'c', 'c', 'Annuaire', 'non', '', '', '', '', '', '', '', '', 'non', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(30, '2014', 'cc', 'c', 'Adulte', 'Masculin', '0000-00-00', 'c', 'c', 'Annuaire', 'non', '', '', '', '', '', '', '', '', 'non', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `partenairesimpliques`
--

CREATE TABLE IF NOT EXISTS `partenairesimpliques` (
  `idPatient` int(11) NOT NULL,
  `structure` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `qualite` varchar(100) NOT NULL,
  `coordonnees` varchar(100) NOT NULL,
  PRIMARY KEY (`idPatient`,`structure`,`type`,`nom`,`qualite`,`coordonnees`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `partenairesimpliques`
--

INSERT INTO `partenairesimpliques` (`idPatient`, `structure`, `type`, `nom`, `qualite`, `coordonnees`) VALUES
(1, 'bbbbaaaa', 'Soins', 'dzqqd', 'qzd', 'qzdqzdq');

-- --------------------------------------------------------

--
-- Structure de la table `partenairesinitiaux`
--

CREATE TABLE IF NOT EXISTS `partenairesinitiaux` (
  `idPatient` int(11) NOT NULL,
  `structure` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `qualite` varchar(100) NOT NULL,
  `coordonnees` varchar(100) NOT NULL,
  PRIMARY KEY (`idPatient`,`structure`,`type`,`nom`,`qualite`,`coordonnees`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `partenairesinitiaux`
--

INSERT INTO `partenairesinitiaux` (`idPatient`, `structure`, `type`, `nom`, `qualite`, `coordonnees`) VALUES
(1, 'a', 'Soins', 'a', 'a', 'a'),
(1, 'aaaaaaaaaaaaaa', 'aaaaaaaa', 'aaaaaa', 'aaaaaaaa', 'aaaaaaaaaaa'),
(1, 'b', 'Soins', 'b', 'b', 'b'),
(1, 'CHU Montpellier', 'Soins', 'zqdqz', 'dzqdqzd', 'qzdqzdqz'),
(9, 'a', 'Soins', 'a', 'a', 'a'),
(9, 'b', 'Soins', 'b', 'b', 'b'),
(9, 'CHU Montpellier', 'Soins', 's', 's', 's'),
(9, 'n', 'Médico-social', 'n', 'n', 'n'),
(24, 'camps carcassonne', 'Soins', 'a', 'a', 'a');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
